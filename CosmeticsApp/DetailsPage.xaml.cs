﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using CosmeticsApp.ViewModels;
using CosmeticsDomain.Cosmetics;
using CosmeticsDomain.Cosmetics.Abstract;
using CosmeticsDomain.Net;
using CosmeticsDomain.Net.Method;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using CosmeticsApp.Resources;
using CosmeticsApp.Ui;

namespace CosmeticsApp
{
    public partial class DetailsPage : PhoneApplicationPage
    {

        public DetailsPage()
        {
            InitializeComponent();
        }

        private class Images
        {
            public static readonly BitmapImage Error;
            public static readonly BitmapImage Valid;
            public static readonly BitmapImage Outdated;

            static Images()
            {
                Error = new BitmapImage();
                Error.SetSource(App.GetResourceStream(new Uri("Assets/ic_error.png", UriKind.Relative)).Stream);

                Valid = new BitmapImage();
                Valid.SetSource(App.GetResourceStream(new Uri("Assets/ic_valid.png", UriKind.Relative)).Stream);

                Outdated = new BitmapImage();
                Outdated.SetSource(App.GetResourceStream(new Uri("Assets/ic_expired.png", UriKind.Relative)).Stream);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (DataContext == null)
            {
                string selectedIndex = "";
                string code = "";
                if (NavigationContext.QueryString.TryGetValue("selectedItem", out selectedIndex) && NavigationContext.QueryString.TryGetValue("code", out code))
                {
                    var brand = App.ViewModel.Items.FirstOrDefault(v => v.ID.Equals(selectedIndex));
                    
                    if(brand != null)
                    {
                        ProgressBar.Show();
                        var loader = new ApiLoader();
                        loader.OnLoadingComplete += LoadCosmetics;
                        loader.OnError += OnError;

                        BrandCaption.Text = brand.LineOne;

                        loader.Load(new CosmeticsInformationApiMethod(code, selectedIndex));
                    }
                    else
                    {
                        OnError(AppResources.LoadError);
                    }
                    
                }
                else
                {
                    OnError(AppResources.Error);
                }
            }
        }

        private void LoadCosmetics(string result)
        {
            ProgressBar.Hide();
            
            var reader = new ApiReader<CosmeticsInformation>(result);

            if(!reader.HaveError)
            {
                if(reader.Cosmetics.Outdated)
                {
                    StatusImage.Source = Images.Outdated;

                    ClearData();

                    Labels.Children.Add(new TextBlock() { Text = AppResources.ProductionDate});
                    Data.Children.Add(new TextBlock() { Text = reader.Cosmetics.Date });

                }
                else
                {
                    StatusImage.Source = Images.Valid;
                    ClearData();

                    Labels.Children.Add(new TextBlock() { Text = AppResources.ValidForNext });
                    Data.Children.Add(new TextBlock() { Text = string.Format("{0} {1}", reader.Cosmetics.ValidUntil,  AppResources.Months)  });


                    Labels.Children.Add(new TextBlock() { Text = AppResources.ProductionDate });
                    Data.Children.Add(new TextBlock() { Text = reader.Cosmetics.Date });

                    Labels.Children.Add(new TextBlock() { Text = AppResources.ProductionShelfLife });
                    Data.Children.Add(new TextBlock() { Text = string.Format("{0} {1}", reader.Cosmetics.ShelfLife, AppResources.Months) });
                }
            }
            else if(reader.ErrorMessage != null)
            {
                OnError(reader.ErrorMessage.Error);
            }
        }

        private void OnError(string message)
        {
            ProgressBar.Hide();
            ClearData();

            BrandCaption.Text = AppResources.Error;
            StatusImage.Source = Images.Error;
            Data.Children.Add(new TextBlock() { Text = message });
        }

        private void ClearData()
        {
            Data.Children.Clear();
            Labels.Children.Clear();
        }
    }
}