﻿using System.Windows;
using System.Windows.Controls;

namespace CosmeticsApp.Ui
{
    public static class CosmeticsProgressBar
    {
        public static void Show(this ProgressBar progressBar)
        {
            progressBar.IsIndeterminate = true;
            progressBar.Visibility = Visibility.Visible;
        }

        public static void Hide(this ProgressBar progressBar)
        {
            progressBar.IsIndeterminate = false;
            progressBar.Visibility = Visibility.Collapsed;
        }
    }
}
