﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using CosmeticsApp.Ui;
using CosmeticsDomain.Cosmetics;
using CosmeticsDomain.Net;
using CosmeticsDomain.Net.Method;

namespace CosmeticsApp.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private ProgressBar _progressBar;


        public MainViewModel()
        {
            this.Items = new ObservableCollection<ItemViewModel>();
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<ItemViewModel> Items { get; private set; }
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding
        /// </summary>
        /// <returns></returns>

        public bool IsDataLoaded
        {
            get;
            private set;
        }

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        /// 

        public void LoadData(ProgressBar progressBar)
        {
            _progressBar = progressBar;
            ProgressBarShow();

            LoadData();
        }

        public void LoadData()
        {
            var apiLoader = new ApiLoader();
            apiLoader.OnError += OnBrandsLoadingError;
            apiLoader.OnLoadingComplete += ExtractCosmeticsAndAppendThemToItems;
            apiLoader.Load(new BrandsApiMethod());
        }

        private void ExtractCosmeticsAndAppendThemToItems(string result)
        {
            ProgressBarHide();

            var brandsReader = new ApiReader<List<BrandItem>>(result);

            var brands = brandsReader.Cosmetics;

            foreach (var brandItem in brands)
            {
                this.Items.Add(new ItemViewModel() { ID = brandItem.Id, LineOne = brandItem.Brand });
            }

            this.IsDataLoaded = true;           
        }

        private void OnBrandsLoadingError(string result)
        {
            ProgressBarHide();
            
            this.Items.Add(new ItemViewModel() { ID = "0", LineOne = result});
            this.IsDataLoaded = true;
        }

        private void ProgressBarShow()
        {
            if(_progressBar != null)
            {
                _progressBar.Show();
            }
        }

        private void ProgressBarHide()
        {
            if (_progressBar != null)
            {
                _progressBar.Hide(); 
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

    }
}