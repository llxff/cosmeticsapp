﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CosmeticsDomain.Cosmetics;

namespace CosmeticsApp.ViewModels
{
    class CosmeticsViewModel
    {
        public string Brand { get; set; }

        public class Information
        {
            public string ShelfLife { get; set; }
            public string DateOfMAnufacturing { get; set; }
            public string ValidUntil { get; set; }

            public bool Outdated { get; set; }            
        }

        public class Error
        {
            public string Message { get; set; }
        }

        public Information Cosmetics { get; set; }
        public Error ErrorMessage { get; set; }
    }
}
