﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CosmeticsDomain.Cosmetics.Abstract;
using Newtonsoft.Json;

namespace CosmeticsDomain.Cosmetics
{
    public class BrandItem : CosmeticsApiInformation
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("brand")]
        public string Brand { get; set; }
    }
}
