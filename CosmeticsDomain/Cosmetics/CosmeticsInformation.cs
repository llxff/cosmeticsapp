﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CosmeticsDomain.Cosmetics.Abstract;
using Newtonsoft.Json;

namespace CosmeticsDomain.Cosmetics
{
    public class CosmeticsInformation : CosmeticsApiInformation
    {
        [JsonProperty("dateOfManufacturer", Required = Required.Default)]
        public string DateOfManufacturer { get; set; }

        [JsonProperty("yearOfManufacturer", Required = Required.Default)]
        public string YearOfManufacturer { get; set; }

        [JsonProperty("shelfLife", Required = Required.AllowNull)]
        public string ShelfLife { get; set; }

        [JsonProperty("validUntil", Required = Required.AllowNull)]
        public string ValidUntil { get; set; }

        [JsonProperty("outdated", Required = Required.AllowNull)]
        public bool Outdated { get; set; }

        public bool HaveFullDate()
        {
            return "".Equals(YearOfManufacturer);
        }

        public string Date { get
        {
            if ("".Equals(DateOfManufacturer))
            {
                return YearOfManufacturer;
            }
            else
            {
                return new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(double.Parse(DateOfManufacturer)).ToShortDateString();
            }
        }}

    }
}
