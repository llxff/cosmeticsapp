﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CosmeticsDomain.Cosmetics;
using CosmeticsDomain.Cosmetics.Abstract;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace CosmeticsDomain.Net
{
    public class ApiReader<T>
    {
        public T Cosmetics { get; private set; }
        public ErrorMessage ErrorMessage { get; private set; }
        public bool HaveError { get; private set; }

        private readonly string _jsonResult;

        public ApiReader(string jsonResult)
        {
            _jsonResult = jsonResult;
            HaveError = false;
            Read();
        }

        private void Read()
        {
            Cosmetics = JsonConvert.DeserializeObject<T>(
                _jsonResult, 
                new JsonSerializerSettings()
                {
                    Error = IfErrorInformation
                    
                }
           );

        }

        private void IfErrorInformation(object sender, ErrorEventArgs args)
        {
            ErrorMessage = JsonConvert.DeserializeObject<ErrorMessage>(
                _jsonResult, 
                new JsonSerializerSettings()
                {
                    Error = IfRecognititionError
                }
            );
            HaveError = true;
            args.ErrorContext.Handled = true;
        }

        private void IfRecognititionError(object sender, ErrorEventArgs args)
        {
            
            ErrorMessage = new ErrorMessage()
                    {
                        Error = "Can't recognize json"
                    };
            HaveError = true;
            args.ErrorContext.Handled = true;
        }
    }
}
