﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CosmeticsDomain.Net.Method.Abstract;

namespace CosmeticsDomain.Net.Method
{
    public class CosmeticsInformationApiMethod : ICosmeticsApiMethod
    {
        private readonly string _code;
        private readonly string _brandNum;

        private const string ApiUrl = "http://107.20.149.226/cosmetics/Home/GetExpirationDate/?code={0}&brandnum={1}";

        public CosmeticsInformationApiMethod(string code, string brandNum)
        {
            _code = code;
            _brandNum = brandNum;
        }

        public string GetUri()
        {
            return string.Format(ApiUrl, _code, _brandNum);
        }
    }
}
