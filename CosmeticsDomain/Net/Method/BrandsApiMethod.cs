﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using CosmeticsDomain.Net.Method.Abstract;

namespace CosmeticsDomain.Net.Method
{
    public class BrandsApiMethod : ICosmeticsApiMethod
    {
        public string GetUri()
        {
            return "http://107.20.149.226/cosmetics/Home/GetBrands";
        }
    }
}
