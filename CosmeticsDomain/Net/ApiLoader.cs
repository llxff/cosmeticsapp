﻿using System;
using System.Collections.Generic;
using System.Net;
using CosmeticsDomain.Cosmetics;
using CosmeticsDomain.Net.Method.Abstract;

namespace CosmeticsDomain.Net
{
    public class ApiLoader
    {       
        public event Action<string> OnLoadingComplete;
        public event Action<string> OnError;
       
        public void Load(ICosmeticsApiMethod method)
        {
            var client = new WebClient();
            client.DownloadStringCompleted += ExtractCosmeticsData;
            client.DownloadStringAsync(new Uri(method.GetUri()));
        }

        private void ExtractCosmeticsData(Object sender, DownloadStringCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null)
            {
                OnLoadingComplete((string)e.Result);
            }
            else
            {
                CallOnError("404");
            }
          
        }

        private void CallOnError(string message)
        {
            if (OnError != null)
            {
                OnError(message);
            }
        }
    }
}
